package Orchestrator;


import java.io.*;
import java.net.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonIOException;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;  



public class MainProcess {
	public static void main(String[] args) throws IOException,NullPointerException, InterruptedException {
	
		NodeObserver JD = new NodeObserver();
		scalability sca = new scalability();
		for(int w=1; w <= 12; w++)
		{
			String hostName = "tn-"+String.valueOf(w);
			sca.Server_addNode(hostName);
			//NodeObserver.nodes.add(new DockerNode(hostName));
			JD.initnoTasks(hostName, 0);
			JD.initHostWeight(hostName, 0);
		}
		
		//============================================
		
	    	//open socket to receive requests from clients on port 4040
			final int port = 5040;
			System.out.println("Server waiting for connection on port "+port);

			@SuppressWarnings("resource")
			ServerSocket ss= new ServerSocket(port);
			Socket clientSocket = ss.accept();
			System.out.println("Recieved connection from "+clientSocket.getInetAddress()+" on port "+clientSocket.getPort());
			
			ResourceAllocation send = new ResourceAllocation(clientSocket);
			Thread thread2 = new Thread(send);
			thread2.start();

			//AWSMonitoring monit = new AWSMonitoring();
			//Thread thread3 = new Thread(monit);
			//thread3.start();
					
	}

}
