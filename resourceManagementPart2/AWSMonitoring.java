package Orchestrator;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;



public class AWSMonitoring implements Runnable{

	private static Map<String, ArrayList<Double>> utilization = new ConcurrentHashMap<String, ArrayList<Double> >();
	
	public void run() {
		NodeObserver JD = new NodeObserver();
		Map<String, String> hosts = JD.getHostNameWID();
		/// initial values
		if(!hosts.isEmpty()) {
				
			// based on the value find the hostname to be return.
			for (Entry<String, String> entry : hosts.entrySet()) 
			{
				ArrayList<Double> p1 = new ArrayList<>();
				p1.add(0.0);
				p1.add(0.0);
				
				utilization.put(entry.getKey(), p1);
			}
		}
		while(true)
		{
			if(!hosts.isEmpty()) {
				ExecutorService executor0 = Executors.newFixedThreadPool(20);
				Future<?> futureTask0 = executor0.submit(() -> {
					// based on the value find the hostname to be return.
					for (Entry<String, String> entry : hosts.entrySet()) 
					{
						//InstanceMonitoring(entry.getValue(),entry.getKey());
						String command1 = " head -n1 /proc/stat ";
						//runDockerCommand(String hostname, String command)
						//removeDockerCommand(String hostname, String command)
						double cpuUsage = monitoringCommand(entry.getKey(), command1);
						//System.out.println("cpuUsage = "+ String.format("%.2f",cpuUsage) + "% Hostname = "+ entry.getKey());
						String command2 = " cat /proc/meminfo ";

						double memUsage = monitoringCommandMem(entry.getKey(), command2);

						Date date = new Date(); // This object contains the current date value
						SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						//System.out.println();
							Writer output;
							try {
							output = new BufferedWriter(new FileWriter("sol1_4.txt", true)); 
							//"$1,$9,$ENDTIME,$runtime,$watingTime,$6,$7,$fitnessValue,$2,$t_4"
							output.append(formatter.format(date)+","+ String.format("%.2f",cpuUsage)+","+ String.format("%.2f",memUsage)+","+ entry.getKey() +"\n");
							
							output.close();

							} catch (IOException e1) {
								e1.printStackTrace();
							}
							
					}
				});
			}
			try{
				Thread.sleep(1000*15);

			} // end of first try 
			catch(Exception ex){
				System.out.println(ex.getMessage());
			}
		}
	}//end run

	public static double monitoringCommand(String hostname, String command){
		
		String cpu_usage = null;
		double DIFFUSAGE= 0;
		try {
			Process process1 = Runtime.getRuntime().exec("docker-machine ssh "+hostname+command);
					
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process1.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process1.getErrorStream()));
			
			String line;
			while ((line = reader.readLine()) != null ) {
				if(line.contains("cpu")) {
					cpu_usage =  line;
				}
			}
			//System.out.println("fit = "+fit.substring(8).replace(" Mapping:",""));
			//System.out.println("map = "+map);
			
			String lineerr;
			while ((lineerr = error.readLine()) != null ) {
				System.out.println(lineerr);
			}
		} catch (IOException e) {
					e.printStackTrace();
		}

		//cpu  15920 638 3637 449271 605 0 64 1488 0 0
		for (Entry<String, ArrayList<Double>> entry : utilization.entrySet()) 
		{
			if(entry.getKey().equals(hostname)){
				double pre_t_total = entry.getValue().get(0); 
				double pre_t_usage = entry.getValue().get(1);
				if(cpu_usage != null){
					//user    nice   system  idle      iowait irq   softirq  steal  guest  guest_nice
					double user= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[0]);
					double nice= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[1]);
					double system= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[2]);
					double idle= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[3]);
					double iowait= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[4]);
					double irq= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[5]);
					double softirq= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[6]);
					double steal= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[7]);
					//double guest= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[8]);
					//double guest_nice= Double.parseDouble(cpu_usage.replace("cpu  ", "").split(" ")[9]);

					double total = user+nice+system+idle+iowait+irq+softirq+steal;
					double t_idle = idle+iowait;

					double t_usage = total - t_idle;
					//double cpuPercentage = (t_usage/total) * 100;
					
					//DIFF_TOTAL=$((TOTAL-PREV_TOTAL))
					double delta_t_usage = t_usage - pre_t_usage;
					double delta_t_total = total - pre_t_total;
					DIFFUSAGE=(delta_t_usage/delta_t_total) * 100;
					addValues(hostname, total, t_usage);
				}
			}
		}
		NodeObserver JD = new NodeObserver();
		if(DIFFUSAGE > 69 ){
			JD.setnodeFlag(hostname, "True");
		}
		else{
			JD.setnodeFlag(hostname, "False");
		}
		
		return DIFFUSAGE;
	}

	public static double monitoringCommandMem(String hostname, String command){
		
		String mem_total = null;
		String mem_free = null;
		double memory_utilization= 0;
		try {
			Process process1 = Runtime.getRuntime().exec("docker-machine ssh "+hostname+command);
					
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process1.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process1.getErrorStream()));
			
			String line;
			while ((line = reader.readLine()) != null ) {
				if(line.contains("MemTotal")) {
					mem_total =  line;
				}
				else if(line.contains("MemFree")) {
					mem_free =  line;
				}
			}
	
			String lineerr;
			while ((lineerr = error.readLine()) != null ) {
				System.out.println(lineerr);
			}
		} catch (IOException e) {
					e.printStackTrace();
		}

		for (Entry<String, ArrayList<Double>> entry : utilization.entrySet()) 
		{
			if(entry.getKey().equals(hostname)){
				if(mem_total != null && mem_free != null){
					
					double memory_total= Double.parseDouble(mem_total.replace("MemTotal:", "").replace("kB","").replace(" ", "").split(" ")[0]);
		
					double memory_free= Double.parseDouble(mem_free.replace("MemFree:", "").replace("kB","").replace(" ", "").split(" ")[0]);


					double mem_utilization = ((memory_total - memory_free) / memory_total) * 100;
					memory_utilization = mem_utilization;
				}
			}
		}	
		return memory_utilization;
	}


	public static void addValues(String hostname, double prevTotal, double prevIdle) {
		ArrayList<Double> p1 = new ArrayList<>();
    	p1.add(prevTotal);
    	p1.add(prevIdle);
		if(hostname != null) {
			if(!utilization.isEmpty()) {
				utilization.put(hostname, p1); 
			}
			else {
				utilization.put(hostname, p1);
			}
		}
	}
	public static String[] getValues(String hostname) {
		
		String ar[] = new String[2];
		double prevTotal = 0.0;
		double prevIdle = 0.0; 

		if(hostname != null) {
			// based on the value find the hostname to be return.
			for (Entry<String, ArrayList<Double>> entry : utilization.entrySet()) 
			{
				if (entry.getKey().equals(hostname)) 
				{
					prevTotal = entry.getValue().get(0); 
					prevIdle = entry.getValue().get(1);
				}
			}
			
		}
		ar[0] = String.valueOf(prevTotal);
		ar[1] =	String.valueOf(prevIdle);

		return ar;
	}

	public static double runDockerCommand(String hostname, String command){
		

		//docker-machine ssh TN-3 "docker run --hostname TN-3  -e value1=1 -e value2=7 -e value3=7 -e value4=2 -e value5=$sampleSeed
		//--name task01 talrefai0/genetic_algorithm"
		
		String cpu_usage = null;

		try {
			Process process1 = Runtime.getRuntime().exec("docker-machine ssh "+hostname+command);
					
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process1.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process1.getErrorStream()));
			
			String line;
			while ((line = reader.readLine()) != null ) {
				if(line.contains("cpu_load:")) {
					cpu_usage =  line;
				}
			}
			//System.out.println("fit = "+fit.substring(8).replace(" Mapping:",""));
			//System.out.println("map = "+map);
			
			String lineerr;
			while ((lineerr = error.readLine()) != null ) {
				System.out.println(lineerr);
			}
		} catch (IOException e) {
					e.printStackTrace();
		}

		double cpuUtilization = Double.parseDouble(cpu_usage.substring(9).replace("cpu_load:",""));
		return cpuUtilization;
	}
	
	public void InstanceMonitoring(String InstID, String hostname) 
	{

		AWSCredentialsProvider awsp = new AWSCredentialsProvider() {

			@Override
			public void refresh() {
				// TODO Auto-generated method stub

			}

			@Override
			public AWSCredentials getCredentials() {
				AWSCredentials awsCredentials = null;
				try {
					awsCredentials = new AWSCredentials() {
						//access-key  AKIAZ2GTXY5TG2JQGR7P --amazonec2-secret-key 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 
						public String getAWSSecretKey() {
							return "rNmCbxVv/7c7nUjHr3k0Soi8nJvevTRc0bGJBMg/";
						}

						public String getAWSAccessKeyId() {
							return "AKIAZ2GTXY5TG2JQGR7P";
						}
					};
				} catch (Exception e) {
					throw new AmazonClientException(
							"can not load your aws credentials, please check your credentials !!", e);
				}
				return awsCredentials;
			}
		};
		try {

			AmazonCloudWatch cw = AmazonCloudWatchClientBuilder.standard().withCredentials(awsp)
					.withRegion("eu-west-1").build();

			Dimension dimension = new Dimension().withName("InstanceId").withValue(InstID);
			long offsetInMilliseconds = 1000 * 60 * 5;// * 24;

			GetMetricStatisticsRequest request = new GetMetricStatisticsRequest()
					.withStartTime(new Date(new Date().getTime() - offsetInMilliseconds)).withNamespace("AWS/EC2")
					.withPeriod(60).withDimensions(dimension).withMetricName("CPUUtilization").withStatistics("Average", "Maximum")
					.withEndTime(new Date());


			GetMetricStatisticsResult getMetricStatisticsResult = cw.getMetricStatistics(request);

			//System.out.println("request StartTime : " + request.getStartTime() + "Hostname " + hostname);
			//System.out.println("request EndTime   : " + request.getEndTime() + "Hostname " + hostname);

			/*
			 * System.out.println("label : " +
			 * getMetricStatisticsResult.getLabel());
			 */
			System.out.println("DataPoint Size : " + getMetricStatisticsResult.getDatapoints().size());
			double MaxcpuUtilization = 0;
			for (final Datapoint dataPoint : getMetricStatisticsResult.getDatapoints()) {
				MaxcpuUtilization = dataPoint.getMaximum();
				System.out.println(dataPoint.getTimestamp() + " MaxcpuUtilization= : " + MaxcpuUtilization + " Hostname " + hostname);
			}

		} catch (AmazonServiceException ase) {

			ase.printStackTrace();
		}
	}			

}