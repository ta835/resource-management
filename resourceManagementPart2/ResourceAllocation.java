package Orchestrator;


import java.io.*;
import java.net.*;
import java.util.Map.Entry;
import java.util.Map;
import java.util.Collections;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.lang.*;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.stat.regression.SimpleRegression;


import java.io.BufferedWriter;
import java.io.Writer;

import com.google.gson.JsonIOException;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;  

import java.util.LinkedList; 
import java.util.Queue; 

import java.text.DecimalFormat;



import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


class ResourceAllocation implements Runnable
{
	Socket clientSock = null;
	Socket sock=null;
	PrintWriter print=null;
	BufferedReader brinput=null;
	BufferedReader brBufferedReader = null;

    // Maximum number of threads in thread pool 
    static final int MAX_T = 154; 	
	public ResourceAllocation(Socket clientSock)
	{
		this.clientSock = clientSock;

	}
	public void run() {

		try{
			//get output & input streams
			this.print = new PrintWriter(clientSock.getOutputStream(),true);
			this.brBufferedReader = new BufferedReader(new InputStreamReader(this.clientSock.getInputStream()));
			
			String messageString;
			while(true)
			{	
					NodeObserver JD = new NodeObserver();


				//assign message from client to messageString
				// then check if the message not equal to null
				
				while((messageString = brBufferedReader.readLine())!= null){
					
					//if the message equal to exit it break to close the connection.
					if(messageString.startsWith("job="))
					{
						
						String msg = messageString.replace("job=", "");
						
						ExecutorService executor0 = Executors.newFixedThreadPool(MAX_T);
						Future<?> futureTask0 = executor0.submit(() -> {
							//===========**********************************
							//===========**********************************
							long time1 =  System.currentTimeMillis();
							
							String jobTemp= msg+";"+ String.valueOf(time1);		
							JD.InsertJopDetails(jobTemp);
							ProcessQueue();
						});
						//executor0.shutdown();

					}// end of else if
					ProcessQueue();
				}// end of while message
			}//end while true
		} // end of first try 
		catch(Exception ex){System.out.println(ex.getMessage());}
	}//end run
	

	//(taskNo, TaskValue, Nogeneration, xdim, ydim, navs, sampleSeed, deadline, time1);
	static void ProcessQueue()
	{
			NodeObserver JD = new NodeObserver();

		// if the message start with JR= then the string can be processed.
		if(JD.getJobPeek() != null){
			//taskNo+";"+ TaskValue+";"+ xdim+";"+ ydim+";"+ navs+";"+FitnessReq+";"+deadline+";"+ String.valueOf(time1);		

			// host with the minimum number of containers.
			//String HOST3 = JD.getHostWithNoConts();
			String HOST3 = JD.getHostWithNSTAD();
			if(HOST3 != null)
			{
				JD.setnoTasksInc(HOST3, 1);
				String msg = JD.getJobDetails();
				String [] token2 = msg.split(";");
				String taskNo = token2[0];
				String TaskValue = token2[1];
				String Nogeneration = String.valueOf(6);
				String xdim = token2[2];
				String ydim = token2[3];
				String navs = token2[4];
				String populationSize = String.valueOf(25);
				String FitnessReq = token2[5];
				int deadline = Integer.valueOf(token2[6]);
				long stime = Long.valueOf(token2[7]); 
		
				String sampleSeed ="[12,7,4,5,4,13,13,6,4,15,7,4,7,9,1,6,0,8,1,16,1,3,3,17,5,2,11,7,5,13,8,8,5,10,9,2,3,3,10,6,0,9,6,8,12,11,16,7,10,13,18,2,18,12,13,6,2,14,4,4,6,17,3,14,1,13,2,2,18,5,5,8,8,4,2,4,14,9,17,7,11,16,15,10,13,8,14,3,0,11,18,7,1,18,4,12,11,17,15,16,15,1,2,1,8,13,17,4,10,1,7,9,16,15,8,10,10,8,0,4,10,7,15,18,3,4,14,0,17,6,6,2,17,6,12,5,7,17,2,3,6,6,11,12,1,7,8,3,9,11,6,5,0,1,13,5,10,15,10,7,3,10,15,2,12,13,5,9,3,12,6,16,7,15,10,18,10,7,4,11,5,12,3,3,12,0,2,1,16,0,12,7,16,11,2,2,12,4,11,8,6,0,17,17,15,14,18,10,2,8,16,2,1,10,14,16,9,4,3,16,12,7,8,15,8,2,0,17,0,14,15,15,0,11]";
				
				ExecutorService executor0 = Executors.newFixedThreadPool(MAX_T);
				Future<?> futureTask0 = executor0.submit(() -> {
					// choose a solution in order to execute the task.
					solution1(HOST3,taskNo, TaskValue, Nogeneration, xdim, ydim, navs,populationSize, sampleSeed, FitnessReq, deadline,stime);
				});
				//executor0.shutdown();
			}
			else{
				//System.out.println("HOST = "+ HOST3  +" Host3 either empty or null..");
			}
		}
		//else{
			//System.out.println("There is no tasks at the moment...");
		//}
	}
	
	//Fitness tracking
	static void solution1(String HOST3, String taskNo, String TaskValue, String Nogeneration, String xdim, String ydim, String navs,String populationSize, String sampleSeed, String FitnessReq,  int deadline, long time1)
	{
		
		NodeObserver JD = new NodeObserver();
		try 
		{
			System.out.println("HOST = "+ HOST3  +" taskNo = "+ taskNo);
			
			double maxfitnessAchieved= 0.0;
			double cpulimit = 1.0;
			
			String finalResult=null;
			double fitnessAchieved=0.0;
			double elaps = 0.0;
			String[] process_1 = null;
			String mapping = null;
			
			//String flag1 = JD.getNodeFlag(HOST3);
			//if(flag1 != null && flag1 == "True"){
			//	cpulimit = limitcpu(deadline, elaps);
			//}
			//else if(flag1 == "False"){
			//	cpulimit = 1.0;
			//}
			//else{
			//	cpulimit = 1.0;
			//}
			
			process_1 = DockerManager.process_task_first_time(HOST3, taskNo, TaskValue, Nogeneration, xdim, ydim, navs, populationSize, sampleSeed, FitnessReq, deadline, time1, cpulimit);	

			elaps=Double.valueOf(process_1[3]);
			finalResult=process_1[0];
			fitnessAchieved = Double.valueOf(process_1[2]);
			maxfitnessAchieved = fitnessAchieved;
			mapping = process_1[1];
			
			writeTaskDetails(taskNo,"Stage-"+String.valueOf(1), "0.0", String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));
			
			//weightCalc(String hostname, int Deadline, int ElapsTime, double preWeight)
			double nstvalue=calcNST(HOST3,deadline,elaps,0.0);	
			//double ndfvalue=calcNDF(HOST3, fitnessAchieved, Double.valueOf(FitnessReq),0.0, maxfitnessAchieved);	
			//double weight=weightCalc(HOST3,deadline,elaps, fitnessAchieved, Double.valueOf(FitnessReq));	
			
			int totalNo_geners=6;
			int max_stage = 20;
			int current_stage=2;
			int jGeneration=6;
			System.out.println("Done 1st stage"+" taskNo = "+ taskNo);

			while(fitnessAchieved > Double.valueOf(FitnessReq) && elaps < deadline && current_stage <= max_stage)
			{
				
				//String flag = JD.getNodeFlag(HOST3);
				//if(flag != null && flag == "True"){
				//	cpulimit = limitcpu(deadline, elaps);
				//}
				//else if(flag1 == "False"){
				//	cpulimit = 1.0;
				//}
				//else{
				//	cpulimit = 1.0;
				//}

				//(String HostName, String URL, String id_cont, String index0, String index1, String NoGenerations, long time1)
				String[] ar2 = DockerManager.process_task_second_time(HOST3, process_1[0],mapping,String.valueOf(jGeneration),time1,populationSize, cpulimit);
				//ar[0] = result1;
				//ar[1] =	Mapping;
				//ar[2] = String.valueOf(fitnessAchieved);
				//ar[3] =	String.valueOf(elaps);
				fitnessAchieved = Double.valueOf(ar2[2]);
				if(fitnessAchieved > maxfitnessAchieved){
					maxfitnessAchieved = fitnessAchieved;
				}
				else{
					break;
				}
				elaps=Integer.valueOf(ar2[3]);
				mapping = ar2[1];
				finalResult = ar2[0];
				totalNo_geners+=jGeneration;

				nstvalue=calcNST(HOST3,deadline,elaps,nstvalue);	
				//ndfvalue=calcNDF(HOST3, fitnessAchieved, Double.valueOf(FitnessReq),ndfvalue,maxfitnessAchieved);
				
				
				writeTaskDetails(taskNo,"Stage-"+current_stage, "0.0", String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));
				
				current_stage++;
			}
			
			removeContandWriteResult(finalResult, totalNo_geners, current_stage-1);
			
			//removeNSTNDF(HOST3,nstvalue, ndfvalue);
			removeNSTNDF(HOST3,0.0, ndfvalue);
			JD.setnoTasksDec(HOST3, 1);
			
			System.out.println("HOST3= "+HOST3 +" finished executed taskNo "+ taskNo );

		} catch (Exception exc) {
        // TODO: handle exception
		}
	}

	static void admissionControlST(String HOST3, String taskNo, String TaskValue, String Nogeneration, String xdim, String ydim, String navs,String populationSize, String sampleSeed, String FitnessReq,  int deadline, long time1)
	{
		NodeObserver JD = new NodeObserver();

		try 
		{
			System.out.println("HOST = "+ HOST3  +" taskNo = "+ taskNo);
			
			double cpulimit = 1.0;
			
			String finalResult=null;
			double fitnessAchieved=0.0;
			double elaps = 0.0;
			String[] process_1 = null;
			String mapping = null;

			process_1 = DockerManager.process_task_first_time(HOST3, taskNo, TaskValue, Nogeneration, xdim, ydim, navs, populationSize, sampleSeed, FitnessReq, deadline, time1, cpulimit);	

			elaps=Double.valueOf(process_1[3]);
			finalResult=process_1[0];
			fitnessAchieved = Double.valueOf(process_1[2]);
			mapping = process_1[1];
			double nstad_value = calcNSTAD(HOST3,deadline,elaps);
			writeTaskDetails(taskNo,"Stage-"+String.valueOf(1), String.valueOf(0.0), String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));

			
			int totalNo_geners=6;
			int current_stage=2;
			int jGeneration=6;
			System.out.println("Done 1st stage"+" taskNo = "+ taskNo);

			while(elaps < deadline && fitnessAchieved > Double.valueOf(FitnessReq))
			{

				//(String HostName, String URL, String id_cont, String index0, String index1, String NoGenerations, long time1)
				String[] ar2 = DockerManager.process_task_second_time(HOST3, process_1[0],mapping,String.valueOf(jGeneration),time1,populationSize, cpulimit);

				fitnessAchieved = Double.valueOf(ar2[2]);
				removeNSTAD(HOST3, nstad_value);
				elaps=Integer.valueOf(ar2[3]);
				mapping = ar2[1];
				finalResult = ar2[0];
				totalNo_geners+=jGeneration;
				
				nstad_value = calcNSTAD(HOST3,deadline,elaps);

				writeTaskDetails(taskNo,"Stage-"+current_stage, String.valueOf(0.0), String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));
				
				current_stage++;
			}
			
			removeContandWriteResult(finalResult, totalNo_geners, current_stage-1);
			
			//removeNSTNDF(HOST3,nstvalue, ndfvalue);
			//removeNSTNDF(HOST3,0.0, 0.0);
			removeNSTAD(HOST3, nstad_value);
			JD.setnoTasksDec(HOST3, 1);
			
			System.out.println("HOST3= "+HOST3 +" finished executed taskNo "+ taskNo );

		} catch (Exception exc) {
        // TODO: handle exception
		}
	}

	public static double calcNSTAD(String hostname, int Deadline, double ElapsTime) 
	{	
		NodeObserver JD = new NodeObserver();
	
		double nstad = ((double) Math.abs(Deadline - ElapsTime)) / ((double) Deadline); 
		JD.sethostsNSTAD(hostname, nstad);

		return nstad;
	}

	public static void removeNSTAD(String hostname, double nstvalue) 
	{
		NodeObserver JD = new NodeObserver();

		JD.sethostNSTAD_Dec(hostname, nstvalue);
	}


	public static double calcNST(String hostname, int Deadline, double ElapsTime, double prenst) 
	{
		NodeObserver JD = new NodeObserver();
		
		double nst = ((double) Math.abs(Deadline - ElapsTime)) / ((double) Deadline); 
		JD.sethostsNST(hostname, nst, prenst);
		///*
		if(nst.isNaN() != true && prenst.isNaN() != true && !Double.isInfinite(nst) && !Double.isInfinite(prenst)){
			
			//System.out.println("nst= "+ String.valueOf(nst) +" Host "+ hostname );
		}
		else if(nst.isNaN() == true && prenst.isNaN() != true){
			JD.sethostsNST(hostname, 0.0, prenst);
		}
		else if(prenst.isNaN() == true && nst.isNaN() != true){
			JD.sethostsNST(hostname, nst, 0.0);
		}
		else{
			JD.sethostsNST(hostname, 0.0, 0.0);
		}
		//*/
		return nst;
	}
	
	public static double calcNDF(String hostname, double FitnessAchieved, double FitnessReq, double prendf, double maxfitnessAchieved) 
	{
		NodeObserver JD = new NodeObserver();
		double ndf = 0.0;
		if(maxfitnessAchieved != 0 )
		{
			ndf = ((double) Math.abs(FitnessAchieved - FitnessReq)) / ((double) maxfitnessAchieved);
		}
		else{
			ndf = ((double) Math.abs(FitnessAchieved - FitnessReq)) / ((double) 1.0);
		}
		
		DecimalFormat df = new DecimalFormat("#.##");      

		JD.sethostsNDF(hostname, Double.valueOf(df.format(ndf)), prendf);
		
		return Double.valueOf(df.format(ndf));
	}
	/////////////////			weightCalcRemove(HOST3, nstvalue, ndfvalue);


	public static void removeNSTNDF(String hostname, double nstvalue, double ndfvalue) 
	{
		NodeObserver JD = new NodeObserver();

		JD.sethostNDFDec(hostname, ndfvalue);
		JD.sethostNSTDec(hostname, nstvalue);
		
	}
	
	public static double limitcpu(int Deadline, double ElapsTime){
		double cpulimit = ElapsTime / Deadline;
		
		return cpulimit;
	}
	
	//******************************************
	
	public static void removeContandWriteResult(String finalResult, int totalNo_geners,int stage)
	{	
		Writer output;
		Writer output2;
		try {
		   output = new BufferedWriter(new FileWriter("sol1_NSTAD.txt", true)); 
		   //"$1,$9,$ENDTIME,$runtime,$watingTime,$6,$7,$fitnessValue,$2,$t_4"
		   output.append(finalResult+","+ String.valueOf(totalNo_geners)+",stage_"+ stage +"\n");
		   
		   output2 = new BufferedWriter(new FileWriter("TA_JobHistory.txt", true));  
		   output2.append(finalResult+","+ String.valueOf(totalNo_geners)+",stage_"+ stage +"\n");
		   output.close();
			output2.close();

		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	
	public static void writeTaskDetails(String TaskID, String stage, String prediction, String fitnessAchieved1,String FitnessReq1,String elaps1,String Deadline01)
	{
		
		String taskDetails = TaskID+","+ stage +","+ prediction+","+ fitnessAchieved1 +","+ FitnessReq1 +
		","+ elaps1+","+ Deadline01+"\n";  
		
		Writer output_3;
		try {
			output_3 = new BufferedWriter(new FileWriter("sol1_3_NSTAD.txt", true)); 

			output_3.append(taskDetails);
		   
			output_3.close();
		} catch (IOException e1) {
		e1.printStackTrace();
		}
				
	}
	
}//end 

