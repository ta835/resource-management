package Orchestrator;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;

import java.util.LinkedList; 
import java.util.Queue; 
  
	
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors; 
import java.util.stream.Stream;
import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

//	JD.sethostNSTAD_Dec(hostname, nstvalue);
//  JD.sethostsNSTAD(hostname, nst);

public class NodeObserver{

	private static Queue<String> queue = new LinkedList<String>();
	// Map that store the hostname as a key and the value is the number of containers	
	
	private static Map<String, Double> hostsWeight = new ConcurrentHashMap<String, Double>();

	private static Map<String, Double> NST = new ConcurrentHashMap<String, Double>();

	private static Map<String, Double> NSTAD = new ConcurrentHashMap<String, Double>();

	private static Map<String, Double> NDF = new ConcurrentHashMap<String, Double>();

	// Map that store the hostname as a key and the value is the url.
	private static Map<String,String> HostsList =  new ConcurrentHashMap<String, String>();
	
	private static Map<String,String> nodeFlag =  new ConcurrentHashMap<String, String>();
	
	private static Map<String,String> HostNameWID = new ConcurrentHashMap<String, String>();
	// Map that store the hostname as a key and the value is the WCET
	private static Map<String, Integer> noTasks = new ConcurrentHashMap<String, Integer>();


	public void InsertJopDetails(String jobT){ 
    	queue.add(jobT);
		
		//sortQueue(queue);
    }
	public String getJobPeek(){
		return queue.peek();	
    }
	public String getJobDetails(){
		return queue.poll();	
    } 
	
	public void initHostNameWID(String hostname, String id) {
		HostNameWID.put(hostname, id);
		System.out.println("hostname = " + hostname + " id = " + id); 
	}

	public Map<String, String> getHostNameWID() {
		return HostNameWID;
	}

/////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
	// this method return the hostname with the minimum number of containers.
	public String getMaxHostNSTAD() {
		String hostname = null;
			// check if the HostMap is not empty
		   if(!NSTAD.isEmpty()) {
			   //System.out.println("I am the JD of ");
			   // go through all the values and store the minimum value in "min"
			   //double max = Collections.max(hostsWeight.values());

			   double min = Collections.min(NSTAD.values());

			   // based on the value find the hostname to be return.
		        for (Entry<String, Double> entry : NSTAD.entrySet()) 
				{
		            if (entry.getValue().equals(min)) 
					{
						if(!Double.isNaN(min))
						{
							hostname = entry.getKey();
							System.out.println("hostname : ************** " + hostname + " min : ************** " + min); 	
						}
						else{
							break;
						}
		            }
		        }
			 }
			 
		   // return the hostname.
	    //System.out.println("hostname : getMaxHostWeight()************** " + hostname);
		return hostname;
	}

	public String getHostWithNSTAD() {
		String hostname = null;
			// check if the HostMap is not empty
		if(!noTasks.isEmpty()) 
			{
			//Collections.shuffle(noTasks);
			for (Entry<String, Integer> entry : noTasks.entrySet()) 
				{
					if(entry.getValue() == 0)
					{
						hostname = entry.getKey();
					}
					else{
						break;
					}
				}
				if(hostname == null){
					System.out.println("hostname : == null =================================== " + hostname);
					hostname = getMaxHostNSTAD();
				}
			}
		// return the hostname.
		//System.out.println("hostname : getMaxHostWeight()************** " + hostname); 		
		return hostname;
	}

//  JD.sethostsNSTAD(hostname, nst);
	public void sethostsNSTAD(String hostname, double st) {
		DecimalFormat df = new DecimalFormat("#.##");      

		if(hostname != null) {
			if(!NSTAD.isEmpty()) {
				double value = NSTAD.get(hostname);
				double temp = (Math.abs (value + st));
				temp = Double.valueOf(df.format(temp));
				NSTAD.put(hostname, temp);			 
			}
			else {
				NSTAD.put(hostname, st);
			}
		}
		else{
			System.out.println("hostname : sethostsNSTAD()-------------- " + hostname);
		}
		//System.out.println("NST: -------------- " + NST);

	}

//	JD.sethostNSTAD_Dec(hostname, nstvalue);
	public void sethostNSTAD_Dec(String hostname, double st) {
		if(hostname != null) {
			if(!NSTAD.isEmpty()) {
				double value = NSTAD.get(hostname);
				NSTAD.remove(hostname);
				NSTAD.put(hostname, Math.abs(value-st)); 
			}
		}
		else{
			System.out.println("hostname : sethostNSTAD_Dec()-------------- might be null " + hostname);
		}
	}
////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

	// this method return the hostname with the minimum number of containers.
	 public String getMaxHostWeight() {
		String hostname = null;
			// check if the HostMap is not empty
		   if(!NDF.isEmpty()) {
			   //System.out.println("I am the JD of ");
			   // go through all the values and store the minimum value in "min"
			   //double max = Collections.max(hostsWeight.values());

			   double min = Collections.min(NDF.values());
			   
			   double infMin = Double.NEGATIVE_INFINITY;

			   // based on the value find the hostname to be return.
		        for (Entry<String, Double> entry : NDF.entrySet()) 
				{
					
		            if (entry.getValue().equals(min)) 
					{
						if(!Double.isNaN(min) && min != infMin)
						{
							hostname = entry.getKey();
							System.out.println("hostname : ************** " + hostname + " min : ************** " + min); 	
						}
						else{
							break;
						}
		            }
		        }
			 }
			 
		   // return the hostname.
	    //System.out.println("hostname : getMaxHostWeight()************** " + hostname); 		

		return hostname;
	}

	// this method return the hostname with 0 number of containers.
	 public String getHostWithNoConts() {
		String hostname = null;
			// check if the HostMap is not empty
		   if(!noTasks.isEmpty()) 
		    {
			   //Collections.shuffle(noTasks);
			   for (Entry<String, Integer> entry : noTasks.entrySet()) 
				{
					if(entry.getValue() == 0)
					{
						hostname = entry.getKey();
					}
					else{
						break;
					}
				}
				if(hostname == null){
					//hostname = getMaxHostWeight();
					System.out.println("hostname : == null =================================== " + hostname); 
				}
				
			}
		   // return the hostname.
		//System.out.println("hostname : getMaxHostWeight()************** " + hostname); 		
		return hostname;
	}
	 // This method will receive hostnames and number of containers then store them
	 // in hostsWeight. 
	public void sethostsWeightInc(String hostname, double tn) {
		if(hostname != null) {
			if(!hostsWeight.isEmpty()) {
				double value = hostsWeight.get(hostname);
				hostsWeight.put(hostname, value+tn); 
			}
			else {
				hostsWeight.put(hostname, tn);
			}
		}
		else{
			System.out.println("hostname : sethostsWeightInc()-------------- " + hostname);
		}
	}
	public void sethostsWeightInc2(String hostname, double tn, double pretn) {
		if(hostname != null) {
			if(!hostsWeight.isEmpty()) {
				double value = hostsWeight.get(hostname);
				double temp = (Math.abs(value - pretn) + tn);
				DecimalFormat df = new DecimalFormat("#.##");      
				temp = Double.valueOf(df.format(temp));
				hostsWeight.put(hostname, temp);
			
			}
			else{
				hostsWeight.put(hostname, tn);
			}			
		}
		else {
			System.out.println("hostname : sethostsWeightInc()-------------- Migh be null" + hostname);
		}
	}

	public void sethostsNST(String hostname, double st, double prest) {
		DecimalFormat df = new DecimalFormat("#.##");      

		if(hostname != null) {
			if(!NST.isEmpty()) {
				double value = NST.get(hostname);
				//if(st.isNaN() != true && prest.isNaN() != true){
					double temp = (Math.abs(value - prest) + st);
					temp = Double.valueOf(df.format(temp));
					NST.put(hostname, temp);
				//}
				//else if(prest.isNaN() == true && st.isNaN() != true){
				//	double temp = ((value - 0.0) + st);
				//	temp = Double.valueOf(df.format(temp));
				//	NST.put(hostname, temp);
				//}
				//else if(prest.isNaN() != true && st.isNaN() == true){
				//	double temp = ((value - prest) + 0.0);
				//	temp = Double.valueOf(df.format(temp));
				//	NST.put(hostname, temp);
				//}				 
			}
			else {
				NST.put(hostname, st);
			}
		}
		else{
			System.out.println("hostname : sethostsNST()-------------- " + hostname);
		}
		System.out.println("NST: -------------- " + NST);
		
	}

	public void sethostNSTDec(String hostname, double st) {
		if(hostname != null) {
			if(!NST.isEmpty()) {
				double value = NST.get(hostname);
				NST.remove(hostname);
				NST.put(hostname, Math.abs(value-st)); 
			}
		}
		else{
			System.out.println("hostname : sethostNSTDec()-------------- might be null " + hostname);
		}
	}
	
	
	public void sethostsNDF(String hostname, double df, double predf) {

		DecimalFormat df2 = new DecimalFormat("#.##");      
		if(hostname != null) {
			if(!NDF.isEmpty()) {
				double value = NDF.get(hostname);
				NDF.remove(hostname);
				double temp = (Math.abs(value - predf) + df);
				temp = Double.valueOf(df2.format(temp));
				NDF.put(hostname, temp);
					 
			}
			
			else {
				NDF.put(hostname, df);
			}
		}
		else{
			System.out.println("hostname : sethostsNDF()-------------- " + hostname);
		}
		System.out.println("NDF: -------------- " + NDF);
		
	}
	
	public void sethostNDFDec(String hostname, double df) {
		if(hostname != null) {
			if(!NDF.isEmpty()) {
				double value = NDF.get(hostname);
				NDF.remove(hostname);
				NDF.put(hostname, Math.abs(value-df)); 
			}
		}
		else{
			System.out.println("hostname : sethostNDFDec()-------------- might be null " + hostname);
		}
	}
	
	public void initHostWeight(String hostname, double tasks) {
		hostsWeight.put(hostname, tasks);
		//System.out.println("hostname = " + wcet); 
	}	
	public void sethostsWeightDec(String hostname, double tn) {
		if(hostname != null) {
			if(!hostsWeight.isEmpty()) {
				double value = hostsWeight.get(hostname);
				hostsWeight.put(hostname, Math.abs(value-tn)); 
			}
			else{
				hostsWeight.put(hostname, tn);
				System.out.println("hostname : sethostsWeightDec()--------------inner if ");
			}
		}
		else{
			System.out.println("hostname : sethostsWeightDec()-------------- " + hostname);
		}
	}

	// return the url from HostsList based on the host name..
	public String getHostsList(String host) {
		 
		String url = null;
   	   	// for loop to get the URL of the hostname from the instance method of class NodeObserver
			for (Entry<String, String> entry : HostsList.entrySet()) 
			{
	            if (entry.getKey().equals(host)) 
				{
	            	url = entry.getValue();
	            }
	        }
		return url;
	}
	 // This method will receive hostnames and the url then store them
	 // in HostsList. 
	public void setHostsList(String host,String url) {
		HostsList.put(host, url);
	}
		// return the url from HostsList based on the host name..
	public String getNodeFlag(String host) {
		 
		String flag = null;
   	   	// for loop to get the URL of the hostname from the instance method of class NodeObserver
			for (Entry<String, String> entry : nodeFlag.entrySet()) 
			{
	            if (entry.getKey().equals(host)) 
				{
	            	flag = entry.getValue();
	            }
	        }
		return flag;
	}
	
	// This method will receive hostnames and the url then store them
	 // in nodeFlag. 
	public void setnodeFlag(String host,String flag) {
		nodeFlag.put(host, flag);
	}	
	public void initnoTasks(String hostname, int tasks) {
		noTasks.put(hostname, tasks);
		NDF.put(hostname, 0.0);
		NST.put(hostname, 0.0);
		NSTAD.put(hostname, 0.0);

		//System.out.println("hostname = " + wcet); 
	}
	// This method will receive hostnames and wcet then store them
	 // in noTasks.
	public void setnoTasksInc(String hostname, int tn) {
		if(hostname != null) {
			if(!noTasks.isEmpty()) {
				int value = noTasks.get(hostname);
				noTasks.put(hostname, value+tn); 
			}
			else {
				noTasks.put(hostname, tn);
			}
		}
		//System.out.println("noTasks = " + noTasks); 
	}
	// This method will receive hostnames and wcet then store them
	 // in noTasks.setSubWCET
	public void setnoTasksDec(String hostname, int tn) {
		 if(hostname != null) {
			 int value = noTasks.get(hostname);
			 noTasks.put(hostname, Math.abs(value-tn));
		}
		//System.out.println("SubWCET = " + noTasks); 
	}

}
