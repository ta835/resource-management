package Orchestrator;

import java.util.List;
import java.io.*;
import java.util.*;
import java.net.*;

import java.util.stream.Collectors;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;

  
public class DockerManager
{ 
    // Maximum number of threads in thread pool 
    static final int MAX_T = 100;              
  
	//String taskNo, String TaskValue, String Nogeneration, String xdim, String ydim, String navs, String sampleSeed, String deadline, long time1
	//process_1 = DockerManager.process_task_first_time(HOST3, taskNo, TaskValue, Nogeneration, xdim, ydim, navs, populationSize, sampleSeed, FitnessReq, deadline, time1);	

	public static String[] process_task_first_time(String HostName, String TaskNo, String TaskValue, String NoGeneration, String Xdim, String Ydim, String NAVS,String populationSize, String SampleSeed, String FitnessReq,  int Deadline, long time1, double cpulimit)
	{
		String ar[] = new String[4];

    	// Format the timestamp provided in the construction method
 		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 	    Date now1 = new Date(time1); 
 	    
 	    // assign the formated timestamp to strDate1
 	    String strDate1 = sdfDate.format(now1);
		
		// value1=no of generation. so when container created can be used as an argument. 
		String value1 = "value1="+NoGeneration;
		// value2= x dim. so when container created can be used as an argument. 
		String value2 = "value2="+Xdim;
		// value3= y dim. so when container created can be used as an argument. 
		String value3 = "value3="+Ydim;
		// value4=navs. so when container created can be used as an argument. 
		String value4 = "value4="+NAVS;
		// value5= population Size. so when container created can be used as an argument. 
		String value5 = "value5="+populationSize;
		// value6= Sample seed. so when container created can be used as an argument. 
		String value6 = "value6="+SampleSeed;
		
		
		// get the current timestamp to be used with the waiting time 
		long wtime = System.currentTimeMillis();
		
		// create container based on the argument provided such as talrefai0/addition:latest, Node-1, IP address, "value=2" 
		// docker run -e value1=150 -e value2=4 -e value3=4 -e value4=1 -e value5=$SeedValue  genetic_algorithm
		//--cpus=1  --memory=1000m
		String command1 = " docker run --cpus="+ String.valueOf(String.format("%.2f",cpulimit)) +"  -e "+value1 +" -e "+value2+" -e "+value3+" -e "+value4+" -e "+value5+" -e "+value6+" --name task_"+TaskNo+" talrefai0/genetic_algorithm";
		//runDockerCommand(String hostname, String command)
		//removeDockerCommand(String hostname, String command)
		String[] com1 = runDockerCommand(HostName, command1);
		long t2 = System.currentTimeMillis();
		double fitnessAchieved = Double.valueOf(com1[0]);
		String Mapping = com1[1];
		long elaps = (t2 - time1)/1000;
		

		long waitingTime = (wtime - time1)/1000;///1000;
		// format variable t2 timestamp as "yyyy-MM-dd HH:mm:ss" 
		Date now2 = new Date(t2);
		//assign the formated timestamp to strDate2
		String strDate2 = sdfDate.format(now2);
		
		String command3 = " docker rm task_"+TaskNo;
		
		
		String result1 = TaskNo+","+strDate1+","+strDate2+","+elaps+","+waitingTime
		+","+Deadline+","+FitnessReq+","+fitnessAchieved+","+Xdim+","+Ydim+","+NAVS
		+","+TaskValue+","+HostName;
		
		removeDockerCommand(HostName, command3);

		ar[0] = result1;
		ar[1] =	Mapping;
		ar[2] = String.valueOf(fitnessAchieved);
		ar[3] =	String.valueOf(elaps);
	
		return ar;
	}

	//String[] ar2 = DockerManager.process_task_second_time(HOST3, process_1[0],mapping,String.valueOf(jGeneration),time1,populationSize);
	//(result_1[0],result_1[1],String.valueOf(y1),5)
	public static String[] process_task_second_time(String HostName, String index0, String index1, String NoGenerations, long time1,String populationSize, double cpulimit)
	{
		String ar[] = new String[4];
 		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String [] token_0 = index0.split(",");
		
		String TaskNo = token_0[0];
		String strDate1 = token_0[1];
		String waitingTime = token_0[4];
		String Deadline = token_0[5];
		String FitnessReq = token_0[6];
		String Xdim = token_0[7];
		String Ydim = token_0[8];
		String NAVS = token_0[9];
		//String fitnessAchieved0 = token_0[10];
		String TaskValue = token_0[11];
		
		

		// value1=no of generation. so when container created can be used as an argument. 
		String value1 = "value1="+NoGenerations;
		// value2= x dim. so when container created can be used as an argument. 
		String value2 = "value2="+Xdim;
		// value3= y dim. so when container created can be used as an argument. 
		String value3 = "value3="+Ydim;
		// value4=navs. so when container created can be used as an argument. 
		String value4 = "value4="+NAVS;
		// value5= population Size. so when container created can be used as an argument. 
		String value5 = "value5="+populationSize;
		// value6= Sample seed. so when container created can be used as an argument. 
		String value6 = "value6="+index1;

		
		// create container based on the argument provided such as talrefai0/addition:latest, Node-1, IP address, "value=2" 
		// docker run -e value1=150 -e value2=4 -e value3=4 -e value4=1 -e value5=$SeedValue  genetic_algorithm
		//--cpus=1 --memory=1000m
		String command1 = " docker run --cpus="+ String.valueOf(String.format("%.2f",cpulimit)) +"  -e "+value1 +" -e "+value2+" -e "+value3+" -e "+value4+" -e "+value5+" -e "+value6+" --name task_"+TaskNo+" talrefai0/genetic_algorithm";
		//runDockerCommand(String hostname, String command)
		//removeDockerCommand(String hostname, String command)
		String[] com1 = runDockerCommand(HostName, command1);
		long t2 = System.currentTimeMillis();
		double fitnessAchieved = Double.valueOf(com1[0]);
		String Mapping = com1[1];
		long elaps = (t2 - time1)/1000;
		

		// format variable t2 timestamp as "yyyy-MM-dd HH:mm:ss" 
		Date now2 = new Date(t2);
		//assign the formated timestamp to strDate2
		String strDate2 = sdfDate.format(now2);
		
		String command3 = " docker rm task_"+TaskNo;
		
		
		String result1 = TaskNo+","+strDate1+","+strDate2+","+elaps+","+waitingTime
		+","+Deadline+","+FitnessReq+","+fitnessAchieved+","+Xdim+","+Ydim+","+NAVS
		+","+TaskValue+","+HostName;
		
		removeDockerCommand(HostName, command3);


		ar[0] = result1;
		ar[1] =	Mapping;
		ar[2] = String.valueOf(fitnessAchieved);
		ar[3] =	String.valueOf(elaps);

		return ar;
	}
	
	public static String[] runDockerCommand(String hostname, String command){
		
		String aw[] = new String[2];
		String fit =null;
		String map = null;
		//docker-machine ssh TN-3 "docker run --hostname TN-3  -e value1=1 -e value2=7 -e value3=7 -e value4=2 -e value5=$sampleSeed
		//--name task01 talrefai0/genetic_algorithm"

		try {
			Process process1 = Runtime.getRuntime().exec("docker-machine ssh "+hostname+command);
					
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process1.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process1.getErrorStream()));
			
			String line;

			while ((line = reader.readLine()) != null ) {
				if(line.contains("Fitness")) {
					fit =  line;
				}
				else if(line.contains("[")) {
				   map =  line;
				}
			}
			//System.out.println("fit = "+fit.substring(8).replace(" Mapping:",""));
			//System.out.println("map = "+map);
			
			String lineerr;
			while ((lineerr = error.readLine()) != null ) {
				System.out.println(lineerr);
			}
		} catch (IOException e) {
					e.printStackTrace();
		}
			
		aw[0] = fit.substring(8).replace(" Mapping:","");
		aw[1] = map;
		return aw;
	}
	
	public static void removeDockerCommand(String hostname, String command){
		try {		
			Process process1 = Runtime.getRuntime().exec("docker-machine ssh "+hostname+command);
					
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process1.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process1.getErrorStream()));
			
			//String line;
			//while ((line = reader.readLine()) != null ) {
			//	System.out.println(line);
			//}
			//System.out.println("fit = "+fit.substring(8).replace(" Mapping:",""));
			//System.out.println("map = "+map);
			
			String lineerr;
			while ((lineerr = error.readLine()) != null ) {
				System.out.println(lineerr);
			}
		} catch (IOException e) {
					e.printStackTrace();
		}
	}	
}
 