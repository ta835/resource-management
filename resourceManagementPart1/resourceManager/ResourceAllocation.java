package Orchestrator;


import java.io.*;
import java.net.*;
import java.util.Map.Entry;
import java.util.Map;
import java.util.Collections;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.lang.*;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import java.io.BufferedWriter;
import java.io.Writer;
import com.google.gson.JsonIOException;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;  
import java.util.LinkedList; 
import java.util.Queue; 

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunctionLagrangeForm;


class ResourceAllocation implements Runnable
{
	Socket clientSock = null;
	Socket sock=null;
	PrintWriter print=null;
	BufferedReader brinput=null;
	BufferedReader brBufferedReader = null;

    // Maximum number of threads in thread pool 
    static final int MAX_T = 100;              
	private Thread schedulUpdater;
	
	private static String mapping = null;
	private static Queue<String> taskQueue = new LinkedList<String>();

	public ResourceAllocation(Socket clientSock)
	{
		this.clientSock = clientSock;

	}
	public void run() {

		try{
			//get output & input streams
			this.print = new PrintWriter(clientSock.getOutputStream(),true);
			this.brBufferedReader = new BufferedReader(new InputStreamReader(this.clientSock.getInputStream()));
			
			String messageString;
			while(true)
			{	

				while((messageString = brBufferedReader.readLine())!= null){
					
					//if the message equal to exit it break to close the connection.
					if(messageString.startsWith("job="))
					{
						String msg = messageString.replace("job=", "");

						ExecutorService executor0 = Executors.newFixedThreadPool(MAX_T);
						Future<?> futureTask0 = executor0.submit(() -> {
							//===========**********************************
							//===========**********************************
							long time1 = System.nanoTime();
							
							String jobTemp= msg+";"+ String.valueOf(time1);		
							InsertTaskDetails(jobTemp);
							ProcessQueue();
						});
						
					}// end of else if
				}// end of while message

			}//end while true
		} // end of first try 
		catch(Exception ex){System.out.println(ex.getMessage());}
	}//end run
	
	public void InsertTaskDetails(String taskD){
    	taskQueue.add(taskD);
    }
    public static String CheckTask(){
		return taskQueue.peek();	
    }
    public static String getTask(){
		return taskQueue.poll();
    }

	//(taskNo, TaskValue, Nogeneration, xdim, ydim, navs, sampleSeed, deadline, time1);
	static void ProcessQueue()
	{
		// if the message start with JR= then the string can be processed.
		if(CheckTask() != null){
			String host1 =  NodeObserver.getHostWithNoConts();
			if(host1 != null){
				String msg = getTask();
				NodeObserver.addTasktohostName(host1,1);
				String [] token2 = msg.split(";");
				String taskNo = token2[0];
				String TaskValue = token2[1];
				String Nogeneration = String.valueOf(6);
				String xdim = token2[2];
				String ydim = token2[3];
				String navs = token2[4];
				String populationSize = String.valueOf(25);
				String FitnessReq = token2[5];
				int deadline = Integer.valueOf(token2[6]);
				long stime = Long.valueOf(token2[7]); 
		
				String sampleSeed ="[12,7,4,5,4,13,13,6,4,15,7,4,7,9,1,6,0,8,1,16,1,3,3,17,5,2,11,7,5,13,8,8,5,10,9,2,3,3,10,6,0,9,6,8,12,11,16,7,10,13,18,2,18,12,13,6,2,14,4,4,6,17,3,14,1,13,2,2,18,5,5,8,8,4,2,4,14,9,17,7,11,16,15,10,13,8,14,3,0,11,18,7,1,18,4,12,11,17,15,16,15,1,2,1,8,13,17,4,10,1,7,9,16,15,8,10,10,8,0,4,10,7,15,18,3,4,14,0,17,6,6,2,17,6,12,5,7,17,2,3,6,6,11,12,1,7,8,3,9,11,6,5,0,1,13,5,10,15,10,7,3,10,15,2,12,13,5,9,3,12,6,16,7,15,10,18,10,7,4,11,5,12,3,3,12,0,2,1,16,0,12,7,16,11,2,2,12,4,11,8,6,0,17,17,15,14,18,10,2,8,16,2,1,10,14,16,9,4,3,16,12,7,8,15,8,2,0,17,0,14,15,15,0,11]";
				
				ExecutorService executor0 = Executors.newFixedThreadPool(MAX_T);
				Future<?> futureTask0 = executor0.submit(() -> {
					// choose a solution in order to execute the task.
					ftApproach(host1,taskNo, TaskValue, Nogeneration, xdim, ydim, navs,populationSize, sampleSeed, FitnessReq, deadline,stime);
				});
			}
		}
	}
	//Fitness tracking
	static void ftApproach(String HOST3, String taskNo, String TaskValue, String Nogeneration, String xdim, String ydim, String navs,String populationSize, String sampleSeed, String FitnessReq,  int deadline, long time1)
	{
		try 
		{
			String filename="sol1_";
			System.out.println("HOST = "+ HOST3 + " taskNo = "+ taskNo);
			String finalResult=null;
			double fitnessAchieved=0.0;
			double elaps = 0.0;
			String[] process_1 = null;
			String mapping = null;
			
			process_1 = DockerManager.process_task_first_time(HOST3, taskNo, TaskValue, Nogeneration, xdim, ydim, navs, populationSize, sampleSeed, FitnessReq, deadline, time1);	
			
			elaps=Double.valueOf(process_1[3]);
			finalResult=process_1[0];
			fitnessAchieved = Double.valueOf(process_1[2]);
			mapping = process_1[1];
			
			writeTaskDetails(filename,taskNo,"Stage-"+String.valueOf(1), "0.0", String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));

			int totalNo_geners=6;
			int max_stage = 20;
			int current_stage=2;
			int jGeneration=6;
			System.out.println("Done 1st stage"+" taskNo = "+ taskNo);

			while(fitnessAchieved > Double.valueOf(FitnessReq) && elaps < deadline && current_stage <= max_stage)
			{
				//(String HostName, String URL, String id_cont, String index0, String index1, String NoGenerations, long time1)
				String[] ar2 = DockerManager.process_task_second_time(HOST3, process_1[0],mapping,String.valueOf(jGeneration),time1,populationSize);
				
				fitnessAchieved = Double.valueOf(ar2[2]);
				
				elaps=Integer.valueOf(ar2[3]);
				mapping = ar2[1];
				finalResult = ar2[0];
				totalNo_geners+=jGeneration;
				
				writeTaskDetails(filename,taskNo,"Stage-"+current_stage, "0.0", String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));
				current_stage++;
			}
			
			contandWriteResult(filename,finalResult, totalNo_geners, current_stage);
			NodeObserver.removeTasktohostName(HOST3, 0);
			System.out.println("HOST3= "+HOST3 +" finished executed taskNo "+ taskNo );
		} catch (Exception exc) {
        // TODO: handle exception
		}
	}
	
	static void fplrApproach(String HOST3, String taskNo, String TaskValue, String Nogeneration, String xdim, String ydim, String navs,String populationSize, String sampleSeed, String FitnessReq,  int deadline, long time1)
	{
		try 
		{
			List<Double> TimeOb = new ArrayList<Double>();
			List<Double> fitness_ob = new ArrayList<Double>();
			
			String filename="sol1_";
			System.out.println("HOST = "+ HOST3 + " taskNo = "+ taskNo);
			String finalResult=null;
			double fitnessAchieved=0.0;
			double elaps = 0.0;
			String[] process_1 = null;
			String mapping = null;
			
			process_1 = DockerManager.process_task_first_time(HOST3, taskNo, TaskValue, Nogeneration, xdim, ydim, navs, populationSize, sampleSeed, FitnessReq, deadline, time1);	
			
			elaps=Double.valueOf(process_1[3]);
			finalResult=process_1[0];
			fitnessAchieved = Double.valueOf(process_1[2]);
			mapping = process_1[1];
			
			writeTaskDetails(filename,taskNo,"Stage-"+String.valueOf(1), "0.0", String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));

			fitness_ob.add(fitnessAchieved);
			TimeOb.add(elaps);	


			int totalNo_geners=6;
			int max_stage = 20;
			int current_stage=2;
			int jGeneration=6;
			System.out.println("Done 1st stage"+" taskNo = "+ taskNo);

			double prediction = Double.valueOf(FitnessReq);
			while(prediction <= Double.valueOf(FitnessReq) && fitnessAchieved > Double.valueOf(FitnessReq) && elaps < deadline && current_stage <= max_stage)
			{
				//(String HostName, String URL, String id_cont, String index0, String index1, String NoGenerations, long time1)
				String[] ar2 = DockerManager.process_task_second_time(HOST3, process_1[0],mapping,String.valueOf(jGeneration),time1,populationSize);
				
				fitnessAchieved = Double.valueOf(ar2[2]);
				
				elaps=Integer.valueOf(ar2[3]);
				mapping = ar2[1];
				finalResult = ar2[0];
				totalNo_geners+=jGeneration;
				
				fitness_ob.add(fitnessAchieved);
				TimeOb.add(elaps);
				
				writeTaskDetails(filename,taskNo,"Stage-"+current_stage, String.valueOf(prediction), String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));
				
				prediction = predict(TimeOb,fitness_ob,deadline);
				
				current_stage++;
			}
			
			contandWriteResult(filename,finalResult, totalNo_geners, current_stage);
			NodeObserver.removeTasktohostName(HOST3, 0);
			System.out.println("HOST3= "+HOST3 +" finished executed taskNo "+ taskNo );
		} catch (Exception exc) {
        // TODO: handle exception
		}
	}
	

	static void curveFittingApproach(String HOST3, String taskNo, String TaskValue, String Nogeneration, String xdim, String ydim, String navs,String populationSize, String sampleSeed, String FitnessReq,  int deadline, long time1)
	{
		try 
		{
			curveFitting predic = new CurveFitting();

	
			String filename="sol1_";
			System.out.println("HOST = "+ HOST3 + " taskNo = "+ taskNo);
			String finalResult=null;
			double fitnessAchieved=0.0;
			double elaps = 0.0;
			String[] process_1 = null;
			String mapping = null;
			
			process_1 = DockerManager.process_task_first_time(HOST3, taskNo, TaskValue, Nogeneration, xdim, ydim, navs, populationSize, sampleSeed, FitnessReq, deadline, time1);	
			
			elaps=Double.valueOf(process_1[3]);
			finalResult=process_1[0];
			fitnessAchieved = Double.valueOf(process_1[2]);
			mapping = process_1[1];
			
			writeTaskDetails(filename,taskNo,"Stage-"+String.valueOf(1), "0.0", String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));
			
			predic.setTimeOb(elaps);
			predic.setFitnessOb(fitnessAchieved);

			int totalNo_geners=6;
			int max_stage = 20;
			int current_stage=2;
			int jGeneration=6;
			System.out.println("Done 1st stage"+" taskNo = "+ taskNo);
			double predictionCF = Double.valueOf(FitnessReq);
			
			while(predictionCF <= Double.valueOf(FitnessReq) && fitnessAchieved > Double.valueOf(FitnessReq) && elaps < deadline && current_stage <= max_stage)
			{
				//(String HostName, String URL, String id_cont, String index0, String index1, String NoGenerations, long time1)
				String[] ar2 = DockerManager.process_task_second_time(HOST3, process_1[0],mapping,String.valueOf(jGeneration),time1,populationSize);
				
				fitnessAchieved = Double.valueOf(ar2[2]);
				
				elaps=Integer.valueOf(ar2[3]);
				mapping = ar2[1];
				finalResult = ar2[0];
				totalNo_geners+=jGeneration;
				
				writeTaskDetails(filename,taskNo,"Stage-"+current_stage, String.valueOf(predictionCF), String.valueOf(fitnessAchieved) , FitnessReq, String.valueOf(elaps), String.valueOf(deadline));
				
				predic.setTimeOb(elaps);
				predic.setFitnessOb(fitnessAchieved);
				predic.process(deadline);
				predictionCF = predic.predict();
				
				current_stage++;
			}
			
			contandWriteResult(filename,finalResult, totalNo_geners, current_stage);
			NodeObserver.removeTasktohostName(HOST3, 0);
			System.out.println("HOST3= "+HOST3 +" finished executed taskNo "+ taskNo );
		} catch (Exception exc) {
        // TODO: handle exception
		}
	}
	
	
	public static double predict(List<Double> TimeOb, List<Double> fitness_ob, int deadline) 
	{
		
		// create a Simple Regression object 
        SimpleRegression simpleRegression = new SimpleRegression();

        simpleRegression.addData(TimeOb.get(TimeOb.size()-2),fitness_ob.get(fitness_ob.size()-2));
	    simpleRegression.addData(TimeOb.get(TimeOb.size()-1),fitness_ob.get(fitness_ob.size()-1));

        // and then you can predict the fitness at given time
        System.out.println("Predicted Fitness: "  + simpleRegression.predict(deadline));
		
        return simpleRegression.predict(deadline);
    }

	//******************************************
	
	public static void contandWriteResult(String filename, String finalResult, int totalNo_geners,int stage)
	{	
		
		String filenamess2 = filename+"2.txt";
		Writer output;
		Writer output2;
		try {
		   output = new BufferedWriter(new FileWriter(filenamess2, true)); 
		   //"$1,$9,$ENDTIME,$runtime,$watingTime,$6,$7,$fitnessValue,$2,$t_4"
		   output.append(finalResult+","+ String.valueOf(totalNo_geners)+",stage_"+ stage +"\n");
		   
		   output2 = new BufferedWriter(new FileWriter("TA_JobHistory.txt", true));  
		   output2.append(finalResult+","+ String.valueOf(totalNo_geners)+",stage_"+ stage +"\n");
		   output.close();
			output2.close();

		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	
	public static void writeTaskDetails(String filename, String TaskID, String stage, String prediction, String fitnessAchieved1,String FitnessReq1,String elaps1,String Deadline01)
	{
		
		String taskDetails = TaskID+","+ stage +","+ prediction+","+ fitnessAchieved1 +","+ FitnessReq1 +
		","+ elaps1+","+ Deadline01+"\n";  

		String filename3 = filename+"3.txt";

		Writer output_3;
		try {
			output_3 = new BufferedWriter(new FileWriter(filename3, true)); 

			output_3.append(taskDetails);
		   
			output_3.close();
		} catch (IOException e1) {
		e1.printStackTrace();
		}
				
	}
	public static void writeTaskDetailsFP2(String TaskID, String stage, String prediction, String fitnessAchieved1,String FitnessReq1,String elaps1,String Deadline01)
	{
		
		String taskDetails = TaskID+","+ stage +","+ prediction+","+ fitnessAchieved1 +","+ FitnessReq1 +
		","+ elaps1+","+ Deadline01+"\n";  
		
		Writer output_3;
		try {
			output_3 = new BufferedWriter(new FileWriter("sol1_3FP2.txt", true)); 

			output_3.append(taskDetails);
		   
			output_3.close();
		} catch (IOException e1) {
		e1.printStackTrace();
		}
		
		
	}

	
}//end
