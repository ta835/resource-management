package Orchestrator;

public class DockerNodes {
    private String Hostname;
    private String instId;

    public DockerNodes(String Hostname) {
    	this.Hostname =Hostname;
    }
      
    public String getHostname() {
    	return this.Hostname;
    }

    public void setHostname(String Hostname) {
    	this.Hostname = Hostname;
    } 

    public String getinstId() {
    	return this.instId;
    }
    public void setinstId(String instId) {
    	this.instId = instId;
    }

    @Override
    public String toString() {
        //return  Hostname + "," + CertPath +","+ DockerConfig +"," + url;
        return  Hostname +","+instId;
    }
}