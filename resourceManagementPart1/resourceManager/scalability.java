package Orchestrator;

import java.io.*;
import java.net.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonIOException;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;  


public class scalability{

	public void Server_addNode(String hostname) { 
		try {		

			Process process1 = Runtime.getRuntime().exec("docker-machine create --driver amazonec2 --amazonec2-region eu-west-1 --amazonec2-zone a"+
			" --amazonec2-instance-type t2.medium --amazonec2-vpc-id vpc-0e72f4daac7d7c74e --amazonec2-subnet-id subnet-0515695ce41ac13f7 --amazonec2-ami ami-0701e7be9b2a77600"+
			" --amazonec2-access-key  AKIAZ --amazonec2-secret-key rNmCbxVv "+
			" --amazonec2-tags=defined_in,manual,group,CS,project,cloud-based_evolutionary_optimisation,pushed_by,bitbucket,status,prod,user,ta835,name,dockerorchestration  "+
			hostname);
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process1.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process1.getErrorStream()));

			String line;
			
			while ((line = reader.readLine()) != null ) {
				System.out.println(line);
			}
			String lineerr;
			while ((lineerr = error.readLine()) != null ) {
				System.out.println(lineerr);
			}


			String command = "/home/ubuntu/ConfigNode.sh "+hostname;
			Process process5 = Runtime.getRuntime().exec(command);
			BufferedReader reader5 = new BufferedReader(
					new InputStreamReader(process5.getInputStream()));
			BufferedReader error5 = new BufferedReader(new InputStreamReader(process5.getErrorStream()));

			String line5;
			while ((line5 = reader5.readLine()) != null) {
				System.out.println(line5 + "\n");
			}
			String lineER5;
			while ((lineER5 = error5.readLine()) != null) {
				System.out.println(lineER5 + "\n");
			}

			String jsonfile = "config1.json";
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// construct Type that tells Gson about the generic type
			Type dtoListType = new TypeToken<List<DockerNodes>>(){}.getType();
			FileReader fr = new FileReader(jsonfile);
			List<DockerNodes> dtos = gson.fromJson(fr, dtoListType);
			fr.close();
			// If it was an empty one create initial list
			if(null == dtos) {
				dtos = new ArrayList<>();
			}
			// Add new item to the list
			dtos.add(new DockerNodes(hostname));
			// No append replace the whole file
			FileWriter fw  = new FileWriter(jsonfile);
			gson.toJson(dtos, fw);
			fw.close();
			
			// init the nodes with 0
			NodeObserver.inithostNames(hostname,0);
	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
				

}
