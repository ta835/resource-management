package Orchestrator;



import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;


public class curveFitting{
	
    private double predictionValue;    // degree of the polynomial regression
    private static List<Double> time_ob = new ArrayList<Double>();
	private static List<Double> fitness_ob = new ArrayList<Double>();
	 
    public curveFitting() {
        
    }
	
	public void setFitnessOb(double y) {
		fitness_ob.add(y);
	}
	

	public void setTimeOb(double x) {
		time_ob.add(x);
	}
	
	
	public void process(int deadline){
		final WeightedObservedPoints obs = new WeightedObservedPoints();

		// ArrayList to Array Conversion 
        for (int i =0; i < time_ob.size(); i++){
            obs.add(time_ob.get(i), fitness_ob.get(i));
		}

        final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(2);

		// Retrieve fitted parameters (coefficients of the polynomial function).
		final double[] coeff = fitter.fit(obs.toList());

        PolynomialFunction polynomial = new PolynomialFunction(coeff);

		double pre_Value = polynomial.value(deadline);

        predictionValue = pre_Value;

	}
	
    public double predict() {

        double y = predictionValue;
        
        return y;
    }
	
}
