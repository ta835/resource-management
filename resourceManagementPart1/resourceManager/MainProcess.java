package Orchestrator;


import java.io.*;
import java.net.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonIOException;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;  



public class MainProcess {
	public static void main(String[] args) throws IOException,NullPointerException, InterruptedException 
	{
	
		String jsonfile = "config1.json";

		scalability sca = new scalability();
		int noNodes= 12;
		BufferedReader reader2 = null;
		try {
			
			reader2 = new BufferedReader(new FileReader(jsonfile));
			Gson gson = new GsonBuilder().create();
			DockerNodes[] Dhosts1 = gson.fromJson(reader2, DockerNodes[].class);
			if(Dhosts1 != null){
				String Hostname = Dhosts1[Dhosts1.length-1].getHostname();
				
				String str = Hostname.replaceAll("\\D+","");
				int breHostname = Integer.valueOf(str);
				for(int w=1; w <= noNodes;w++)
				{
					String hostName = "tn"+String.valueOf(w+breHostname);
					sca.Server_addNode(hostName);
				}
			}
			else{
				for(int w=1; w <= noNodes;w++)
				{
					String hostName = "tn"+String.valueOf(w);
					sca.Server_addNode(hostName);
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		}

		//============================================
			
		//open socket to receive requests from clients on port 4040
		final int port = 5040;
		System.out.println("Server waiting for connection on port "+port);

		@SuppressWarnings("resource")
		ServerSocket ss= new ServerSocket(port);
		Socket clientSocket = ss.accept();
		System.out.println("Recieved connection from "+clientSocket.getInetAddress()+" on port "+clientSocket.getPort());
		
		ResourceAllocation send = new ResourceAllocation(clientSocket);
		Thread thread2 = new Thread(send);
		thread2.start();

		//ExecuteJobFromQ send3 = new ExecuteJobFromQ();
		//Thread thread3 = new Thread(send3);
		//thread3.start();
						
	}

}
