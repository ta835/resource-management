package Orchestrator;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;

import java.util.LinkedList; 
import java.util.Queue; 
  
	
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors; 
import java.util.stream.Stream;
import java.util.*;


public class NodeObserver{

	
	// Map that store the hostname as a key and the value is the WCET	
	private static Map<String, Integer> hostNames = new LinkedHashMap<String, Integer>();


 
	// this method return the hostname with 0 number of containers.
	 public static String getHostWithNoConts() {
		String hostname = null;
			// check if the HostMap is not empty
		   if(!hostNames.isEmpty()) 
		    {
			   //Collections.shuffle(hostNames);
			   for (Entry<String, Integer> entry : hostNames.entrySet()) 
				{
					if(entry.getValue() == 0)
					{
						hostname = entry.getKey();
					}
				}
			}
		   // return the hostname.
		return hostname;
	}

	public static void inithostNames(String hostname, int task) {
		hostNames.put(hostname, task);
	}
	// This method will receive hostnames and wcet then store them
	 // in hostNames.
	public static void addTasktohostName(String hostname, int task) {
		if(hostname != null) {
			hostNames.put(hostname, task);
		}
	}
	public static void removeTasktohostName(String hostname, int task) {
		if(hostname != null) {
			hostNames.put(hostname, task);
		}
	}
}
