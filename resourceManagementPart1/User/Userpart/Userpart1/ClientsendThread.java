package Userpart.Userpart1;


import java.io.*;
import java.net.*;

class ClientsendThread implements Runnable
{
	Socket sock=null;
	PrintWriter print=null;
	BufferedReader brinput=null;
	
	
	
	public ClientsendThread(Socket sock)
	{
		this.sock = sock;
	}//end constructor
	
	public void run(){
		try{
			// check if there is a connection with the server.	
			if(sock.isConnected()) 
			{
			// print the address and the port number
			System.out.println("Client connected to "+sock.getInetAddress() + " on port "+sock.getPort());
						
			this.print = new PrintWriter(sock.getOutputStream(), true);	
			
			while(true){
				try {
				// read tasks from file jobRequest.txt
				BufferedReader in = new BufferedReader(new FileReader("jobRequest.txt"));
			    String str;
			    //check if the line not equal to null
			    while ((str = in.readLine()) != null) {
					//1;88;4;3;2;0;39
					//id,value,x,y,navs,FF,deadline
					String [] token2 = str.split(";");
					String taskNo = token2[0];
					String TaskValue = token2[1];
					String xdim = token2[2];
					String ydim = token2[3];
					String navs = token2[4];
					String FitnessReq = token2[5];
					String deadline = token2[6];
					
					int jobWaiting = Integer.valueOf(token2[7]);

					Thread.sleep(jobWaiting*1000);
					//System.out.println(str);
				
					
					String jobTemp= taskNo+";"+ TaskValue+";"+ xdim+";"+ ydim+";"+ navs+";"+FitnessReq+";"+deadline;
					this.print.println("job="+jobTemp);
					System.out.println("job="+jobTemp);

			    }
			    this.sock.close();
				System.exit(0);
			}
			catch (IOException e) {
			    System.out.println("File Read Error");
			}
			this.print.flush();
			
			// print a confirmation that the job was sent.
			System.out.println("Job submitted...");

			}//end while
		}}catch(Exception e){
			System.out.println(e.getMessage());}
	}//end run method

	

}//end class