package Userpart.Userpart1;


import java.io.*;
import java.net.*;

public class Client {
	public static void main(String[] args) throws UnknownHostException, NullPointerException, IOException, InterruptedException
	{
		// connect to server using IP address and 4040 as port number.
	    Socket sock = new Socket("ip-10-0-30-85.eu-west-1.compute.internal",5040);
		

	    // create an instance of class ClientsendThread class
	    ClientsendThread sendThread = new ClientsendThread(sock);
		Thread thread = new Thread(sendThread);
		thread.start();
	}

}
