# resource management

This project offers ways to manage multiple instances of genetic algorithms (GAs) running as containers over a cloud environment. This project aims to obtain a GA output that achieves a user-defined fitness level by a user-defined deadline for as many instances as possible. The proposed approaches allocate the GA containers to cloud nodes and carefully control the execution of every GA instance by forcing them to run in iterations to reach such a goal.


## Requirements:


- two AWS EC2 instances (type t2.micro) for the experiments (1 VCPU - 1 GiB memory - OS: ubuntu)
    This instance is for the user to send the task and 
    the other instance is for the resource manager to receive and allocate the tasks.

- On the resource manager instance:
    * install the Docker engine to create
        a Docker machine and handle the execution of the tasks.

- on the user instance:
    no preinstallation is required as it is just for sending the tasks to the other 
    instance(resource manager).

## Instructions:

1. In the resource allocation java file ("ResourceAllocation.java"), we need
    to choose the approach that needs to be used in the experiment.

2. Run command
    java -cp .:lib/* Orchestrator.MainProcess
    in the terminal to start the resource manager which then starts by creating
    the nodes (Docker machines) that will be used in the experiment.
    After each node creation, the applications will be downloaded using "ConfigNode.
    sh" which handles the downloading on each node.

3. Once step two is done, on the user instance we need to run the command
    java Userpart.Client
    Experimental platform, metrics and methods
    which will connect to the resource manager instance and start sending the
    tasks.